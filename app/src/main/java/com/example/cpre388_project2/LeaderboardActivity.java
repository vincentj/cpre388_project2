package com.example.cpre388_project2;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cpre388_project2.Controller.LeaderboardAdapter;
import com.example.cpre388_project2.Models.Profile;
import com.example.cpre388_project2.Models.Score;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Activity that starts the leaderboard view and queries firebase for the 10 highest scores.
 */
public class LeaderboardActivity  extends AppCompatActivity {
    //Firebase instance
    /**
     * private Firebase DatabaseReference
     */
    private DatabaseReference mFirebaseDatabaseReference;
    //Adapter instance
    /**
     * private LeaderboardAdapter Instance
     */
    private LeaderboardAdapter adapter;

    /**
     * Override for the leaderboard that gets the firebase reference
     * @param savedInstanceState savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);
        // Initialize Firebase
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        //Create the recyclerview
        RecyclerView recyclerView = findViewById(R.id.leaderboard);
        adapter = new LeaderboardAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //Grab the current data
        Query q = mFirebaseDatabaseReference.child("profile").orderByChild("highestScore").limitToLast(10);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for(DataSnapshot child : snapshot.getChildren())
                    {
                        //Stores the highest 10 scores
                        Profile p = child.getValue(Profile.class);
                        adapter.addScore(new Score(p.getUsername(),p.getHighestScore()));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }

        });

    }
}
