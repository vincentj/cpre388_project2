package com.example.cpre388_project2.Models;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.TextView;

/**
 * Class that implements the sensor listening for the Gyro
 */
public class Sensors implements SensorEventListener {

    //Sensor Instance variables
    private SensorManager sensorManager;
    private Sensor mSensorGyro;
    private Sensor mSensorAccelerometer;
    private float yrot = 0;
    private long timestamp = 0;
    private static final float NS2S = 1.0f / 1000000000.0f;

    /**
     * Initializes the sensors given the sensor manager
     * @param sensorManager sensorManager
     */
    public void initSensors(SensorManager sensorManager)
    {
        this.sensorManager = sensorManager;
        mSensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mSensorGyro = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        if(mSensorAccelerometer != null)
        {
            sensorManager.registerListener(this, mSensorAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        }
        if(mSensorGyro != null)
        {
            sensorManager.registerListener(this, mSensorGyro, SensorManager.SENSOR_DELAY_GAME);
        }
    }

    /**
     * On every change of the gyro sensors, integrates the value of rotation if high enough.
     * @param event SensorEvent
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        int sensorType = event.sensor.getType();


        switch(sensorType)
        {
            case Sensor.TYPE_LINEAR_ACCELERATION:
                break;
            case Sensor.TYPE_GYROSCOPE:

                if(timestamp!= 0)
                {
                    final float dT = (event.timestamp - timestamp) * NS2S;
                    float axisY = event.values[1];

                    //Sensor change is too small to count
                    if(Math.abs(axisY * dT) <= 0.001)
                    {
                        //do nothing
                    }
                    else {
                        yrot += axisY * dT;
                    }
                }
                timestamp = event.timestamp;
                break;
            default:
        }
    }


    /**
     * Get the current y rotation
     * @return
     */
    public float getRot()
    {
        return yrot;
    }

    /**
     * Empty method to ignore sensor accuracy changes.
     * @param sensor Sensor
     * @param i int
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        //Do nothing
    }
}
