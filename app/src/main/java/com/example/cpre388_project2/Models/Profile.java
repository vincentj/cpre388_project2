package com.example.cpre388_project2.Models;

/**
 * Firebase model to store highscores and login information
 */
public class Profile {
    private String username;
    //Probably do something else with this if we have time
    private String password;
    private int highestScore;

    /**
     * Empty Constuctor
     */
    public Profile() {
    }

    /**
     * Constructor for Profile
     * @param username String
     * @param password String
     * @param score int
     */
    public Profile(String username, String password, int score) {
        this.username = username;
        this.password = password;
        highestScore = score;
    }

    /**
     * Return Username
     * @return username String
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set new Username
     * @param username String
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Return password
     * @return String password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set new Password
     * @param password String
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get highest score
     * @return int
     */
    public int getHighestScore() {
        return highestScore;
    }

    /**
     * Set new High Score
     * @param highestScore int
     */
    public void setHighestScore(int highestScore) {
        this.highestScore = highestScore;
    }
}
