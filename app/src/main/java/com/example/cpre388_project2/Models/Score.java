package com.example.cpre388_project2.Models;

/**
 * Model to store score for the Leaderboard Adapter
 */
public class Score {
    //private variables
    private String username;
    private int score;

    /**
     * Constructor for new Score
     * @param username String
     * @param score int
     */
    public Score(String username, int score) {
        this.username = username;
        this.score = score;
    }

    /**
     * Empty constructor.
     */
    public Score() {
    }

    /**
     * Return username
     * @return String
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set new Username
     * @param username String
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Return Score value
     * @return int
     */
    public int getScore() {
        return score;
    }

    /**
     * Set new Score value
     * @param score int
     */
    public void setScore(int score) {
        this.score = score;
    }
}
