package com.example.cpre388_project2.Models;

import android.graphics.RectF;

import java.util.Random;

/**
 * Ball Class
 */
public class Ball {

    private RectF rectangle;
    private float xVelocity;
    private float yVelocity;

    //size of ball;
    private int length = 10;
    private int height = 10;

    private int screenWidth;
    private int screenHeight;

    private int ballWidth = 10;
    private int ballHeight = 10;

    float relativeX;
    float relativeY;

    /**
     * Ball Constructor
     * sets the velocity of the ball,
     * creates a new rectangle object of size 10x10
     * @param screenWidth of the view
     * @param screenHeight of the view
     */
    public Ball(int screenWidth, int screenHeight) {
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        xVelocity = 5;
        yVelocity = 5;
        rectangle = new RectF();
    }

    /**
     *
     * @return returns the rectangle objet
     */
    public RectF getRectangle() {
        return rectangle;
    }

    /**
     * Sets the Pos of the ball
     */
    public void setPos() {
        rectangle.left = rectangle.left + xVelocity;
        rectangle.top = rectangle.top + yVelocity;
        rectangle.right = rectangle.left + length;
        rectangle.bottom = rectangle.top + height;
    }

    /**
     * Reverse the XVelocity
     */
    public void flipXVelocity() {
        xVelocity = -xVelocity;
    }

    /**
     * Reverse the YVelocity
     */
    public void flipYVelocity() {
        yVelocity = -yVelocity;
    }

    /**
     * Set the Velocity of the Ball
     */
    public void setVelocity() {
        Random random = new Random();
        if(random.nextInt(2) == 0) {
            flipXVelocity();
        }
    }

    /**
     * Clears obstacle on Y
     * @param y
     */
    public void clearObstacleOnY(float y) {
        rectangle.bottom = y;
        rectangle.top = y - relativeY;
    }

    /**
     * Clears obstalce on X
     * @param x
     */
    public void clearObstacleOnX(float x) {
        rectangle.left = x;
        rectangle.right = x + relativeX;
    }

    /**
     * Reset the ball position with given parameters
     * @param x
     * @param y
     */
    public void resetBallPos(int x, int y) {
        rectangle.left = x /20;
        rectangle.top =  y - 20;
        rectangle.right = x / 2 + ballWidth;
        rectangle.left = y - 20 - ballHeight;
    }

}
