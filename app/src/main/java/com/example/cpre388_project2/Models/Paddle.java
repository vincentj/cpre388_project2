package com.example.cpre388_project2.Models;

import android.graphics.RectF;

/**
 * Paddle Class
 */
public class Paddle {
    private RectF rectangle;

    //Size of the Paddle
    private int length = 160;
    private int height = 20;

    private int screenWidth;
    private int screenHeight;

    //Speed of the Paddle
    private float speed;

    private float relativeX;
    private float relativeY;

    /**
     * Constructor for Paddle Class
     * sets the shape of the paddle.
     * @param screenWidth of the view from GameView
     * @param screenHeight of the view from GameView
     */
    public Paddle(int screenWidth, int screenHeight) {
        this.screenWidth = screenWidth;
        relativeX = screenWidth / (float) 2 - (float) length/2;
        relativeY = screenHeight - 20;
        rectangle = new RectF(relativeX, relativeY, relativeX+length, relativeY+height);
        speed = 15;
    }

    /**
     * getRectangle() method
     * @return returns the rectangle object
     */
    public RectF getRectangle(){
        return rectangle;
    }

    /**
     * Set the position of the paddle,
     * also checks to see if the paddle is on either side of the screen
     * @param rot
     */
    public void setRot(float rot) {
        System.out.println(rectangle.left);
        if (rectangle.left < 0) {
            relativeX = screenWidth - length;
            rectangle.left = relativeX;
            rectangle.right = relativeX + length;
        } else if (rectangle.right > screenWidth + 1) {
            rectangle.left = 0;
            rectangle.right =  length;
            relativeX = 0;
        } else {
            relativeX += (rot * speed);
            rectangle.left = relativeX;
            rectangle.right = relativeX + length;
        }
    }
}
