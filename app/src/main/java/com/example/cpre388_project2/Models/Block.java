package com.example.cpre388_project2.Models;

import android.graphics.RectF;

/**
 * Block Class
 */
public class Block {

    private RectF rect;
    private boolean isVisible;

    /**
     * Block Constructor
     * @param row is for a double array position
     * @param column is for a double array position
     * @param width of the block
     * @param height of the block
     */
    public Block(int row, int column, int width, int height) {
        isVisible = true;
        rect = new RectF(column * width + 1,
                row * height + 1,
                column * width + width - 1,
                row * height + height - 1);
    }

    /**
     * returns the rect object
     * @return
     */
    public RectF getRect() {
        return rect;
    }

    /**
     * Method sets the block Invisible from the view
     */
    public void setInvisible() {
        isVisible = false;
    }

    /**
     * Returns the visibility of the block
     * @return
     */
    public boolean getVisibility() {
        return isVisible;
    }
}
