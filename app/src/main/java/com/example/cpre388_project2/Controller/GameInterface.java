package com.example.cpre388_project2.Controller;

/**
 * Interface to end the game and return the score
 */
public interface GameInterface {
    /**
     * Ends the game and returns information back to MainActivity
     * @param score int
     */
    void endGame(int score);
}
