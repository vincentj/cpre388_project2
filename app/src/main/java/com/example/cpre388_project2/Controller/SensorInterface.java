package com.example.cpre388_project2.Controller;

/**
 * Sensor interface to read sensor information from the gameView
 */
public interface SensorInterface {
    /**
     * Returns the current paddle rotation
     * @return float
     */
    float getPaddleRot();
}
