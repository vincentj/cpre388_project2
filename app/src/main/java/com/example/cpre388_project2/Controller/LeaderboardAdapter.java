package com.example.cpre388_project2.Controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cpre388_project2.Models.Score;
import com.example.cpre388_project2.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Leaderboard adapter that is used to display the top ten scores from firebase
 */
public class LeaderboardAdapter extends RecyclerView.Adapter<LeaderboardAdapter.BoardViewHolder>{

    //Instance variables
    private final LayoutInflater mInflater;
    private List<Score> scoreList;

    /**
     * Constructor that creates a score list and sets the layout
     * @param context Context
     */
    public LeaderboardAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        scoreList= new ArrayList<Score>();

    }

    /**
     * Layout inflating when the view holder is created for leaderboard items.
     * @param parent ViewGroup
     * @param viewType int
     * @return BoardViewHolder
     */
    @NonNull
    @Override
    public LeaderboardAdapter.BoardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.leaderboard_item, parent, false);
        return new BoardViewHolder(view);
    }

    /**
     * Displays all of the scores from the database by converting to score list items
     * @param holder ViewHolder
     * @param position int
     */
    @Override
    public void onBindViewHolder(@NonNull BoardViewHolder holder, int position) {
        if(scoreList != null || scoreList.isEmpty()) {
            //Grab the score at that position
            Score current = scoreList.get(position);
            holder.username.setText(current.getUsername());
            holder.score.setText(((Integer)current.getScore()).toString());

        }
        else
        {
            //If firebase fails
            holder.username.setText("NO");
            holder.score.setText("Scores");
        }
    }


    /**
     * Return score list size. Should be 10 unless fewer profiles exist
     * @return int
     */
    @Override
    public int getItemCount() {
        return scoreList.size();
    }

    public void setLeaderboardList(List<Score> scoreList)
    {
        this.scoreList = scoreList;
    }

    /**
     * Add a new score to the list of scores.
     * @param score new Score
     */
    public void addScore(Score score) {
        scoreList.add(0,score);
        //if changed
        notifyDataSetChanged();
    }

    /**
     * View holder inner class to store the username and password
     */
    class BoardViewHolder extends RecyclerView.ViewHolder{
        private final TextView username;
        private final TextView score;

        private BoardViewHolder(View view) {
            super(view);
            username = view.findViewById(R.id.leaderUsername);
            score = view.findViewById(R.id.leaderScore);
        }
    }
}
