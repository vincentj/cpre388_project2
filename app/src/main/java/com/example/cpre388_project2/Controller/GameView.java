package com.example.cpre388_project2.Controller;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cpre388_project2.Controller.SensorInterface;
import com.example.cpre388_project2.GameActivity;
import com.example.cpre388_project2.Models.Ball;
import com.example.cpre388_project2.Models.Block;
import com.example.cpre388_project2.Models.Paddle;

import java.util.Random;

import static com.example.cpre388_project2.GameActivity.*;

/**
 * GameView Class, this class builds the view for the game
 * also has the collision logic
 */
public class GameView extends  SurfaceView implements Runnable {

    SensorInterface sensors;
    GameInterface game;
    //Threads to run the game
    Thread gameThread;

    private int level = 1;
    private static int lives = 100;
    private static int score = 0;
    private boolean paddle_collision = true;

    //
    SurfaceHolder holder;

    private boolean validSurface = false;
    private boolean playing = false;

    Block[] blocks = new Block[200];
    int numBlocks;
    int numBlocksLeft;
    int blockColor1;
    int blockColor2;
    int blockColor3;

    private Canvas canvas;
    private Paint paint;

    private Paddle paddle;
    private Ball ball;

    private int screenX;
    private int screenY;

    /**
     *setInterface method for Sensors
     * @param s
     * @param g
     */
    public void setInterface(SensorInterface s, GameInterface g)
    {sensors = s; game = g;}

    /**
     *GameView Constructor
     * initializes a SurfaceHolder, Cavnas and Paint objects
     * Creates a new Thread and start that thread.
     * @param context
     */
    public GameView(Context context) {
        super(context);
        //Intialize Canvas and Paint Objects
        holder = getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {
                validSurface = true;
            }

            @Override
            public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {

            }
        });
        paint = new Paint();


        gameThread = new Thread(this);
        gameThread.start();
    }

    /**
     * Run method checks the status of the game
     * and returns the gamescore once the game is finished
     */
    @Override
    public void run() {
        while(!validSurface) {
            System.out.println("NOSURFACE");
        }

        screenX = getWidth();
        screenY = getHeight() - 147;

        if (!playing) {
            score = 0;
            setUp();
        }

        while(playing) {
            if ((playing) && (numBlocksLeft == 0)) {
                level++;
                setUp();
            }
            draw();
            update();
        }
        //When game ends, send score back to activity and end run.
        game.endGame(score);
        return;

    }

    /**
     * Update the position of the ball, paddle
     * and check for collisions, on blocks, walls, and the paddle.
     */
    private void update() {
        //update paddles pos
        if(sensors != null)
        {
            float rot = sensors.getPaddleRot();
            paddle.setRot(rot);
        }
        //ball pos
        ball.setPos();

//        //Collision Checks on Blocks
        for (int i = 0; i < numBlocks; i++) {
            if(blocks[i].getVisibility()) {
                if (RectF.intersects(blocks[i].getRect(), ball.getRectangle())) {
                    blocks[i].setInvisible();
                    ball.flipYVelocity();
                    numBlocksLeft--;
                    score += 10;
                }
            }
            paddle_collision = false;
        }

        //Collision on Paddle
        if (RectF.intersects(paddle.getRectangle(), ball.getRectangle())) {
            paddle_collision = true;
            ball.setVelocity();
            ball.flipYVelocity();
            ball.clearObstacleOnY(paddle.getRectangle().top - 10);
        }

        //Collision on Bottom of Screen
        if (ball.getRectangle().bottom > screenY) { //-10
            ball.flipYVelocity();
            ball.clearObstacleOnY(screenY - 22); //-22
            if (!paddle_collision) {
                lives--;
            }
            if (lives == 0) {
                playing = false;
            }
        }

        //Collision on Top of Screen
        if (ball.getRectangle().top < 0) {
            ball.flipYVelocity();
            ball.clearObstacleOnY(12);
            paddle_collision = false;
        }

        //Collision on Left of Screen
        if (ball.getRectangle().left < 0) {
            ball.flipXVelocity();
            ball.clearObstacleOnX(12);
            paddle_collision = false;
        }

        //Collision on Right of Screen
        if (ball.getRectangle().right > screenX - 10) {
            ball.flipXVelocity();
            ball.clearObstacleOnX(screenX - 22);
            paddle_collision = false;
        }
    }

    /**
     * draw the our objects to the screen
     */
    public void draw() {

        // Make sure our drawing surface is valid or we crash
        if (holder.getSurface().isValid()) {
            // Lock the canvas ready to draw
            canvas = holder.lockCanvas();
            if(canvas == null)
            {
                return;
            }
            // Draw the background color
            canvas.drawColor(Color.argb(255,  26, 128, 182));

            // Choose the brush color for drawing
            paint.setColor(Color.argb(255,  255, 255, 255));

            if(paddle != null)
            {
                //paddle = new Paddle(screenX, screenY);
                RectF p = paddle.getRectangle();
                // Draw the paddle
                canvas.drawRect(p, paint);
            }

            // Draw the ball
            RectF b = ball.getRectangle();
            canvas.drawRect(b, paint);

            paint.setColor(Color.argb(255,  blockColor1, blockColor2, blockColor3));
            // Draw the bricks
            if (blocks != null) {
                for (int i = 0; i < numBlocks; i++) {
                    if (blocks[i].getVisibility()) {
                        canvas.drawRect(blocks[i].getRect(), paint);
                    }
                }
            }

            // Draw the HUD
            paint.setColor(Color.argb(255, 0,0, 0));
            paint.setTextSize(40);
            canvas.drawText("Score: " + score + " Lives: " + lives + " Level:" + level, 10, 50, paint);

            // Draw everything to the screen
            holder.unlockCanvasAndPost(canvas);
        }

    }

    private void setUp() {
        lives = 3;
        paddle = new Paddle(screenX, screenY);
        ball = new Ball(screenX, screenY);
        ball.resetBallPos(screenX, screenY);
        int blockWidth = screenX / 8;
        int blockHeight = screenY / 10;
        Random random = new Random();
        blockColor1 = random.nextInt(225);
        blockColor2 = random.nextInt(255);
        blockColor3 = random.nextInt(255);
        numBlocks = 0;
        for (int column = 0; column < 8; column++) {
            for (int row = 0; row < level; row ++) {
                blocks[numBlocks] = new Block(row, column, blockWidth, blockHeight);
                numBlocks++;
            }
        }
        numBlocksLeft = numBlocks;
        playing = true;
    }

    /**
     * Set the playing boolean to false
     * to end the game early
     */
    public void setFinished() {
        playing = false;
    }
}