package com.example.cpre388_project2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cpre388_project2.Models.Profile;
import com.example.cpre388_project2.Models.Score;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * This is the activity that queries firebase for the profile information and can create and delete their account
 */
public class ProfileActivity extends AppCompatActivity {
    // Firebase instance variables
    /**
     * private DatabaseReference
     */
    private DatabaseReference mFirebaseDatabaseReference;
    /**
     * public Profile table String for firebase
     */
    public static final String PROFILE = "profile";

    /**
     * onCreate method override that gets the database reference
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        // Get Reference
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
    }

    /**
     * This method signs the user up with the current information from the new text fields.
     * Called by the signup button
     * @param view  View
     */
    public void signUp(View view)
    {
        final EditText username = findViewById(R.id.UserInput);
        EditText password = findViewById(R.id.PasswordInput);
        final Profile newProfile = new Profile(username.getText().toString(), password.getText().toString(), 0);
        //Create a new user in database
        mFirebaseDatabaseReference.child(PROFILE).push().setValue(newProfile).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Intent data = new Intent();
                data.putExtra("username", newProfile.getUsername());
                data.putExtra("pass", newProfile.getPassword());
                data.putExtra("hs", "" +  newProfile.getHighestScore());
                setResult(Activity.RESULT_OK,data);
                finish();
            }
        });
    }


    /**
     * This method deletes the profile of the user matching the text fields if one exists.
     * Called by the delete profile button.
     * @param view View
     */
    public void deleteProfile(View view)
    {
        final EditText username = findViewById(R.id.UserInput);
        final EditText password = findViewById(R.id.PasswordInput);
        Query q = mFirebaseDatabaseReference.child(PROFILE).orderByChild("username").equalTo(username.getText().toString());
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    for(DataSnapshot child : snapshot.getChildren())
                    {
                        Profile p = child.getValue(Profile.class);
                        if (p.getPassword().equals(password.getText().toString())) {
                            //Account exists
                            child.getRef().removeValue();
                            Toast.makeText(ProfileActivity.super.getApplicationContext(), "Delete Success", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                else
                {
                    //Fail
                    CharSequence x = "Failed to Delete";
                    Toast.makeText(ProfileActivity.super.getApplicationContext(), x, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                //Fail
                CharSequence x = "Failed to Delete";
                Toast.makeText(ProfileActivity.super.getApplicationContext(), x, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Method that signs the user in using the information from the given fields
     * Called by the signin Buttion.
     * @param view View
     */
    public void signIn(View view)
    {
        final EditText username = findViewById(R.id.UserInput);
        final EditText password = findViewById(R.id.PasswordInput);
        Query q = mFirebaseDatabaseReference.child(PROFILE).orderByChild("username").equalTo(username.getText().toString());
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    for(DataSnapshot child : snapshot.getChildren())
                    {
                        Profile p = child.getValue(Profile.class);
                        if (p.getPassword().equals(password.getText().toString())) {
                            //Login success send back login information
                            Intent data = new Intent();
                            data.putExtra("username", p.getUsername());
                            data.putExtra("pass", p.getPassword());
                            data.putExtra("hs", "" + p.getHighestScore());
                            setResult(Activity.RESULT_OK, data);
                            finish();
                        }
                    }
                }
                else
                {
                    //Fail
                    CharSequence x = "Failed to Login";
                    Toast.makeText(ProfileActivity.super.getApplicationContext(), x, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                //Fail
                CharSequence x = "Failed to Login";
                Toast.makeText(ProfileActivity.super.getApplicationContext(), x, Toast.LENGTH_LONG).show();
            }
        });

    }

    /**
     * Method that allows the user to update their password on their profile.
     * Takes the input from the text fields to attempt to sign in.
     * If successful, then the user's name will be updated in firebase based on the text in the alert dialog
     * @param view View
     */
    public void updateProfile(View view)
    {
        final EditText username = findViewById(R.id.UserInput);
        final EditText password = findViewById(R.id.PasswordInput);
        Query q = mFirebaseDatabaseReference.child(PROFILE).orderByChild("username").equalTo(username.getText().toString());
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists())
                {
                    for(DataSnapshot child : snapshot.getChildren())
                    {
                        final Profile p = child.getValue(Profile.class);
                        if (p.getPassword().equals(password.getText().toString())) {
                            //Login success
                            //update Password need to be final
                            final String[] newPassword = {null};
                            //Create alert for new password
                            AlertDialog.Builder alert = new AlertDialog.Builder(ProfileActivity.this);
                            alert.setMessage("Enter New Password");
                            final EditText text = new EditText(ProfileActivity.this);
                            alert.setView(text);
                            alert.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //update it in here
                                    newPassword[0] = text.getText().toString();

                                    //Get the database
                                    DatabaseReference mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
                                    Query q = mFirebaseDatabaseReference.child("profile").orderByChild("username").equalTo(p.getUsername());
                                    q.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                                            if (snapshot.exists()) {
                                                for(DataSnapshot child : snapshot.getChildren())
                                                {
                                                    //update the password in the database
                                                    p.setPassword(newPassword[0]);
                                                    child.getRef().setValue(p);
                                                    Toast.makeText(ProfileActivity.this, "Password Updated: ", Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError error) {

                                        }

                                    });
                                    //Send back intent with user Information (to update scores)
                                    Intent data = new Intent();
                                    data.putExtra("username", p.getUsername());
                                    data.putExtra("pass", newPassword[0]);
                                    data.putExtra("hs", "" + p.getHighestScore());
                                    setResult(Activity.RESULT_OK, data);
                                    finish();
                                }
                            });
                            alert.show();


                        }
                    }
                }
                else
                {
                    //Fail
                    CharSequence x = "Failed to Login";
                    Toast.makeText(ProfileActivity.this, x, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                //Fail
                CharSequence x = "Failed to Login";
                Toast.makeText(ProfileActivity.this, x, Toast.LENGTH_LONG).show();
            }
        });

    }
}
