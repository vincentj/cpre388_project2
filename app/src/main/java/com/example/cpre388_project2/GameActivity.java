package com.example.cpre388_project2;

import android.app.Activity;
import android.content.Intent;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cpre388_project2.Controller.GameInterface;
import com.example.cpre388_project2.Controller.GameView;
import com.example.cpre388_project2.Controller.SensorInterface;
import com.example.cpre388_project2.Models.Sensors;

import java.net.URI;

/**
 * The activity that is launched that controls the GameView and returns the score and user information if applicable back to the main activity.
 * Implements the interfaces for sensors and the Game.
 */
public class GameActivity extends AppCompatActivity implements SensorInterface, GameInterface {

    //Instance variables
    /**
     * private Sensors instance
     */
    private Sensors sensors;
    /**
     * private GameView instance
     */
    private GameView gameView;

    /**
     * Creates an instance of the game view and sets up the sensors
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //init sensors
        sensors = new Sensors();
        sensors.initSensors((SensorManager) getSystemService(SENSOR_SERVICE));
        //init gameview
        gameView = new GameView(this);
        gameView.setInterface(this, this);
        setContentView(gameView);

    }

    /**
     * Implementation of the Sensor Interface that retrieves sensor rotation information
     * @return float
     */
    @Override
    public float getPaddleRot() {
        return sensors.getRot();
    }

    /**
     * Implementation of the Game Interface that returns the score information to the Main Activity class
     * @param score int
     */
    @Override
    public void endGame(int score) {
        Intent data = new Intent();
        data.setData(Uri.parse("" + score));
        setResult(Activity.RESULT_OK,data);
        finish();
    }


    /**
     * Override the back pressed to end the game first
     */
    @Override
    public void onBackPressed() {
        gameView.setFinished();
        super.onBackPressed();
    }

    /**
     * Override onOptionsItemSelected to end game before returning on backpress
     * @param item MenuItem
     * @return boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
