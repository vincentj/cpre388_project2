package com.example.cpre388_project2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.cpre388_project2.Controller.GameView;
import com.example.cpre388_project2.Controller.SensorInterface;
import com.example.cpre388_project2.Models.Profile;
import com.example.cpre388_project2.Models.Sensors;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

/**
 * MainActivity that controls the main menu and facilitates between the 3 other activities (leaderboard, Game, Profile)
 */
public class MainActivity extends AppCompatActivity{

    //Sores the results numbers for on activity result
    private static final int PROFILE_RESULTS = 90;
    private static final int GAME_RESULTS = 91;
    private Profile profile;

    /**
     * OnCreate override that shows the main menu information
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    /**
     * Starts the game activity for result to fetch the score
     * @param view View
     */
    public void startGame(View view)
    {
        Intent i = new Intent(this, GameActivity.class);
        startActivityForResult(i, GAME_RESULTS);
    }

    /**
     * Starts the profile activity for result and fetches the profile information
     * @param view View
     */
    public void profile(View view)
    {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivityForResult(intent, PROFILE_RESULTS);
    }

    /**
     * Starts the leaderboard activity
     * @param view View
     */
    public void leaderboard(View view)
    {
        Intent intent = new Intent(this, LeaderboardActivity.class);
        startActivity(intent);
    }

    /**
     * On activity result override to get profile and score information to update the leaderboard.
     * @param requestCode activity Code
     * @param resultCode Result status
     * @param data Data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from profile
        if(resultCode == Activity.RESULT_OK) {
            if (requestCode == PROFILE_RESULTS) {
                String username = data.getStringExtra("username");
                String password = data.getStringExtra("pass");
                int hs = Integer.parseInt(data.getStringExtra("hs"));
                profile = new Profile(username, password, hs);
                Toast.makeText(this, "Login Success", Toast.LENGTH_SHORT).show();
            }
            else if(requestCode == GAME_RESULTS)
            {
                int score = Integer.parseInt(data.getDataString());
                Toast.makeText(this, "Score: " + score, Toast.LENGTH_SHORT).show();
                updateHighScore(score);
                setContentView(R.layout.activity_main);
            }
        }
        else
        {
            if(requestCode == GAME_RESULTS)
            {
                Toast.makeText(this, "Error in Game", Toast.LENGTH_SHORT).show();
            }

        }
    }

    /**
     * Method called on Game completion that updates the score for the current user if the score is higher than the current highest score
     * @param newScore New Score Value
     */
    public void updateHighScore(final int newScore)
    {
        if(profile == null)
        {
            Toast.makeText(this, "No profile found", Toast.LENGTH_LONG).show();
            return;
        }
        //double check that the score is actually higher
        if(profile.getHighestScore() < newScore) {
            //Query the database
            DatabaseReference mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
            Query q = mFirebaseDatabaseReference.child("profile").orderByChild("username").equalTo(profile.getUsername());
            q.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        for(DataSnapshot child : snapshot.getChildren())
                        {
                            //Updates the score in database
                            profile.setHighestScore(newScore);
                            child.getRef().setValue(profile);
                            Toast.makeText(MainActivity.super.getApplicationContext(), "Score Updated to: " + newScore, Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }

            });
        }
        else
        {
            Toast.makeText(this, "Score lower than high score", Toast.LENGTH_LONG).show();
        }
    }

}